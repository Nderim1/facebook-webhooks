const db = require('../db')

function getNotifications () {
	return db.facebook_webhooks.find()
}

function saveNotification (data) {
	return db.facebook_webhooks.save(data)
}

module.exports = {
	getNotifications,
	saveNotification
}