FROM node

RUN npm i gulp -g

WORKDIR /usr/src/app
ADD . /usr/src/app
RUN npm i

EXPOSE 3333

CMD ["sh", "-c", "gulp"]