const HttpError = require('http-errors')
const webhooks = require('../models/webhookNotifications')
const util = require('util')
const _ = require('lodash')
const rp = require('request-promise')

console.inspect = (obj) => console.log(util.inspect(obj, {showHidden: false, depth: 8}))

function shouldBeSaved(body) {
	const dataTypes = ['status', 'photo', 'album']

	if (dataTypes.indexOf(_.get(body, 'entry[0].changes[0].value.item')) > -1 )
		return body
}

function verifyToken (req, res, next) {
	if (req.query['hub.verify_token'] === 'testWebhooks')
		return res.send(req.query['hub.challenge'])
	res.status(400).render('index', {title: 'Set a correct verify token!'})
}

function postWebhook (req, res, next) {
	const dataToSave = shouldBeSaved(req.body)
	if( dataToSave ) {
		webhooks.saveNotification(dataToSave)
			.then((response) => {
				console.log('<--- notification was saved on db --->')
				res.status(200).send('OK')
				return rp({
					method: 'POST',
					uri: process.env.SITE_SYNCER + '/api/sync',
					json: {
						id: _.get(response, 'entry[0].changes[0].value.sender_id').toString()
					}
				})
				.then(() => {
					console.log('******** ---> Site synced! <--- *********')
				})
				.catch((err) => {
					console.error(err.message)
				})
			})
			.catch((err) => {
				return next(
					HttpError(err.status || 500, err.message || 'Cannot save data on db!')
				)
			})
	} else {
		return Promise.resolve(res.status(200).send('Nothing to save'))
	}
}

function getNotifications (req, res, next) {
	webhooks.getNotifications()
		.then((notifications) => {
			console.inspect(notifications)
			res.status(200).render('index', {title: 'Data from db:', data: `${JSON.stringify(notifications, undefined, 4)}`})
		})
		.catch((err) => {
			console.log(err)
			return next(HttpError(err.status || 500))
		})
}

module.exports = {
	verifyToken,
	postWebhook,
	getNotifications
}