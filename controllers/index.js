const express = require('express');
const router = express.Router();
const controller = require('./controller')
const HttpError = require('http-errors')

router.get('/', (req, res) => {
	res.render('index', { title: 'Facebook Webhook' })
});

router.route('/webhook')
	.get( controller.verifyToken )
	.post( controller.postWebhook )

router.get('/notifications', controller.getNotifications)

router.use(( req, res, next ) => {
	return next(HttpError(404))
})

router.use((err, req, res) => {
	console.error(err.stack || err)
	let error = err.status ? err : HttpError(500)
	if(process.env.NODE_ENV === 'development') error.stack = err.stack
	res.status(error.status).render('error')
})

module.exports = router